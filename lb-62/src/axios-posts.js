import axios from 'axios'

const instance = axios.create({
    baseURL:'https://lb-62-b8342.firebaseio.com/'
});
export default instance;