import React, {Component} from 'react';
import axios from "../../axios-posts";
import Nav from "./Nav";

class Edit extends Component {
    state = {
        title :'',
        text :'',
    };

    valueChanged =(event) =>{
        const name = event.target.name;
        this.setState({[name]:event.target.value})
    };


    componentDidMount() {
        const id=this.props.match.params.id;
        axios.get(`/posts/${id}.json`).then(response=>{
            this.setState({title: response.data.title, text: response.data.text});
        })
    }

    publishHandler = () => {
        const id=this.props.match.params.id;
        axios.put(`/posts/${id}.json`, this.state).then(response=>{
            this.props.history.push('/');
        })
    }

    render() {
        return (
            <div className='post'>
                <Nav></Nav>
                <h2>Add new post</h2>
                <div className='post-box'>
                    <input className='post-title' value={this.state.title} onChange={this.valueChanged}
                           type="text" placeholder="Type post title here" name="title"/>
                    <textarea id="postbody" value={this.state.text} onChange={this.valueChanged}
                              cols="30" rows="10" name="text"/>
                    <button className='post-btn' onClick={this.publishHandler}>Save</button>
                </div>
            </div>
        )
            ;
    }
}


export default Edit;